#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#define RO 600
#define pi 3.14
#define g 9.8

double mytrack (char track[20], double x)
{
    double r;
    if(!strcmp(track,"sin"))
    {
        r=sin(x);
        return r;
    }

    if(!strcmp(track,"cos"))
    {
        r=cos(x);
        return r;
    }

    return -666.666;
}

void falldown(double x0[3], double y0[3], double k, char track[20])
{
    int i,c;
    double t, tp, yt, h;
    double y[3],x[3],v[3];

    yt=k+k*mytrack(track,x0[0]);
    h=y0[0]-0.5-yt;
    t=sqrt(2*h/g);
    c=0;
    printf("%lf %lf\n",t,h);

    for (i=1;i<3;i++)
    {
        yt=k+k*mytrack(track,x0[i]);
        h=y0[i]-0.5-yt;
        printf("%lf %lf\n", sqrt(2*h/g),h);
        if (t>sqrt(2*h/g))
        {
            t=sqrt(2*h/g);
            c=i;
        }
    }

    printf("%lf\n%d\n",t,c);

    for(i=0;i<3;i++)
    {
        y[i]=y0[i]-0.5;
    }

    tp=0;
    while (y[c]-(k+k*mytrack(track,x0[c]))>0.0001)
    {
        for (i=0;i<3;i++)
        {
            y[i]-=g*tp*tp/2;
            v[i]=-g*tp;
        }
        tp+=0.000001;
       //printf("%lf %lf %lf %lf %lf\n",k+k*mytrack(track,x0[c]),g*tp*tp/2,y[0],y[1],y[2]);
    }
    printf("%lf %lf %lf %lf %lf\n",k+k*mytrack(track,x0[c]),g*tp*tp/2,y[0],y[1],y[2]);
    printf("\nOne wheel is on track\n\n");

    double r[3],l,lt;
    int p,j,o;

    r[0]=sqrt((x0[0]-x0[1])*(x0[0]-x0[1])+(y[0]-y[1])*(y[0]-y[1]));
    r[1]=sqrt((x0[0]-x0[2])*(x0[0]-x0[2])+(y[0]-y[2])*(y[0]-y[2]));
    r[2]=sqrt((x0[2]-x0[1])*(x0[2]-x0[1])+(y[2]-y[1])*(y[2]-y[1]));

    printf("%lf %lf %lf\n",r[0],r[1],r[2]);

    lt=0;
    for (i=0;i<3;i++)
    {
        if(i!=c)
        {
            l=x0[i]-x0[c];
            if (l<0) l*=-1;
            if (lt<l)
            {
                lt=l;
                p=i;
            }
        }
    }

    printf("%lf %d\n",lt,p);

    if (c==0)
    {
        if (p==1)
            j=0;
        else
            j=1;
    }
    else
        j=2;
    printf("%d\n",j);

   // printf("%lf\n",k+k*mytrack(track,x0[2]-0.001));

   for (i=0;i<3;i++)
   {
       x[i]=x0[i];
   }

    double x2,y2,a,b,d,fi;

    a=1.0001;
    b=x[c]+0.02*(y[c]-0.01);
    d=(y[c]-0.01)*(y[c]-0.01)-r[j]*r[j];

    x2=(b+sqrt(b*b-4*a*d))/(2*a);
    y2=k+k*mytrack(track,x2);
    printf("x1=%lf\n",x2);
    printf("y1=%lf\n",y2);

    fi=sqrt((x[p]-x2)*(x[p]-x2)+(y[p]-y2)*(y[p]-y2))/r[j];
    printf("\n%lf\n",fi);

    double dx;

    printf("%lf\n",y[p]-y2);
    t=(y[p]-y2)/0.000001;
    printf("%lf\n",t);
    printf("%lf\n",x[p]-x2);
    dx=(x[p]-x2)/t;
    printf("%lf\n",dx);
    for(i=0;i<3;i++)
        if (i!=c && i!=p) o=i;
    for (i=0;i<floor(t);i++)
    {
        y[p]-=0.000001;
        x[p]-=dx;

    }
    printf("%lf %lf %lf %lf\n",x[p],x2,y[p],y2);

    printf("\nSecond wheel is on track\n");
}

int main()
{
    FILE *f;
    int i;
    double x0[3],y0[3],k,a,b;
    double r[3],m[3];
    double btime,etime,xt,yt,mcx,mcy,mc;
    char track[20];

    btime=clock();

    if((f=fopen("data.txt","r"))==NULL)
    {
        printf("Can't open data file\n\n");
        return 666;
    }

    for(i=0;i<3;i++)
    {
        fscanf(f,"%lf %lf",&x0[i],&y0[i]);
        printf("%.3lf %.3lf\n",x0[i],y0[i]);
    }

    fscanf(f,"%lf %lf %lf",&k,&a,&b);
    printf("%.3lf\n%.3lf %.3lf\n",k,a,b);

    fgetc(f);

    fgets(track,20,f);
    printf("%s\n",track);

    fclose(f);

    mc=0;
    for(i=0;i<3;i++)
    {
        r[i]=1;
        m[i]=r[i]*r[i]*pi*RO;
        mc+=m[i];
    }

    /*mcx=(m[1]*x0[1]+m[2]*x0[2]+m[3]*x0[3])/mc;
    mcy=(m[1]*y0[1]+m[2]*y0[2]+m[3]*y0[3])/mc;
    printf("%.6lf %.6lf\n",mcx,mcy);*/

    /*for(xt=a;xt<=b;xt+=0.001)
    {
        yt=mytrack(track,xt);
        printf("%7.3lf ",yt);
    }

    etime=clock();

    printf("\n\n%lf\n",(etime-btime)/CLK_TCK);
*/

    falldown(x0,y0,k,track);

    return 0;
}
